import { Dish } from './dish';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
  // use of template
  // template: `<h1>{{title}}</h1>`
})
export class HomePage {

  title: string = "Top Dishes in India";
  // 'Dal', 'Paratha', 'Biriyani', 'Mutton Chops', 'Dosa', 'Roti', 'Butter Chicken', 'Naan', 'Puri'
  dishes = [
    new Dish(1, 'Dal'),
    new Dish(2, 'Paratha'),
    new Dish(3, 'Biriyani'),
    new Dish(4, 'Mutton Chops'),
    new Dish(5, 'Dosa'),
    new Dish(6, 'Roti'),
    new Dish(7, 'Butter Chicken'),
    new Dish(8, 'Naan'),
    new Dish(9, 'Puri')
  ];
  myFav: Dish = this.dishes[3];  

  constructor() {
    console.log(this.myFav)
  }

}
